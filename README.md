# Prueba de Aptitud en Línea - Frontend Angular #

Nota: Si el tiempo es una limitación, intente resolver lo que considera la parte más valiosa del ejercicio. Es mejor concentrar los esfuerzos y mostrar la calidad de su trabajo en una parte del ejercicio que resolver todo con mala calidad. En tal caso, describa cómo manejaría las partes del ejercicio que no puede entregar, confiando en un archivo Readme.md.

Este ejercicio se realizará fuera de Maiq


### Enunciado ###

* Consumir el web service de themoviedb Developers y haciendo uso de la api-key proporcionada para autenticación,
* Obtener el listado de películas e implementar un componente que permita la visualización del listado de películas (tabla, listas, tarjetas, slide, etc...)
* Adicional a esto se debe proporcionar una vista de detalle que permita ver la información completa de la película.

### Requisitos: ###

* Se debe crear un componente lista de películas de la página 1.
* Ejemplo: https://api.themoviedb.org/3/movie/now_playing?api_key=abe3c20e4784c1548ebd1bef45bb392d&page=1
* Se debe crear un componente para el detalle de cada ítem en el listado de películas, y este componente se debe iterar y cargar con la información proveniente del web service.
* Se debe manejar un único componente para el detalle de cada película y re utilizar para ver el detalle de las diferentes películas.
* Debe existir un botón atrás en las vista del detalle de cada reseña que permita volver a visualizar el listado de reseñas de películas.
* Use mediaQueries para adaptar el diseño en pantallas pequeñas. (No se proporciona diseños, así que usa tu creatividad).
* Puede confiar en preprocesadores / transpiladores como LESS / SASS o cualquier tipo de marco CSS si lo desea. (CSS puro también es una opción, pero debe argumentar los beneficios)
---
### datos Api...

> **API-Key:**  abe3c20e4784c1548ebd1bef45bb392d 

---
#### Url:

https://api.themoviedb.org/3/movie/550?api_key=abe3c20e4784c1548ebd1bef45bb392d

---
### Diseño HTML-CSS ###

* Fuente web: Lato
* Eres libre de aplicar tus estilos, pero el diseño debe ser 100% responsive.
* Muéstranos un vistazo de lo que te gusta hacer (y saber lo mejor de ti). 👊👊👊





### Entrega ###
* Fecha de entrega viernes 29 de noviembre 12:00 medio día.
* Envíe un enlace a jhonatan.acelas@maiq.co, camilo.castillo@maiq.co, rodrigo@maiq.co apuntando a su repositorio (Github, Bitbucket).
* Utilizar Angular 7 o superior para la solución.